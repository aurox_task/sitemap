package generator

import (
	"net/url"
	"testing"

	. "github.com/onsi/ginkgo"
	. "github.com/onsi/gomega"
)

const ValidUrl = "https://name.com"
const ValidLoc = "docs"
const InvalidLoc = "very_looooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooong_string"

func TestGenerator(t *testing.T) {
	RegisterFailHandler(Fail)
	RunSpecs(t, "Generator_Test")
}

var _ = Describe("Map Generator", func() {
	//BeforeSuite(func() {
	//	ctx, repo := t.CreateTestRepo()
	//
	//	broker := t.CreateBroker(AdminTestBrokerSymbol)
	//	_, err := repo.AddBroker(ctx, broker)
	//	Expect(err).To(BeNil())
	//
	//})
	//
	//AfterSuite(func() {
	//	ctx, repo := t.CreateTestRepo()
	//
	//	broker := t.CreateBroker(AdminTestBrokerSymbol)
	//	_, err := repo.Db(ctx).Query(&broker, "DELETE FROM broker WHERE symbol = ?", broker.Symbol)
	//	Expect(err).To(BeNil())
	//
	//})

	Context("Map Generator", func() {
		//var ctx context.Context
		var gen *Generator
		//var admin *models.Admin
		//var broker *models.Broker
		//var bErr api.BasicError

		BeforeEach(func() {
			gen = CreateTestGenerator()
			//	broker = t.CreateBroker(AdminTestBrokerSymbol)
			//	admin = t.CreateAdmin(broker)
			//
		})

		//AfterEach(func() {
		//	ctx, repo := t.CreateTestRepo()
		//
		//	var err error
		//
		//	_, err = repo.Db(ctx).Query(admin, "DELETE FROM admin WHERE id = ?", admin.Id)
		//	Expect(err).To(BeNil())
		//
		//})

		It("Valid loc", func() {
			ok, err := gen.AddLoc(ValidLoc)
			Expect(err).To(BeNil())
			Expect(ok).To(BeTrue())

		})

		It("Valid loc twice", func() {
			_, _ = gen.AddLoc(ValidLoc)
			ok, err := gen.AddLoc(ValidLoc)
			Expect(err).To(BeNil())
			Expect(ok).To(Not(BeTrue()))

		})

		It("Invalid loc", func() {
			_, err := gen.AddLoc(InvalidLoc)
			Expect(err).To(Not(BeNil()))

		})

		//It("Invalid url scheme", func() {
		//	args := CreateTestArgsInvalidUrlScheme()
		//	err := args.Validate()
		//	Expect(err).To(Not(BeNil()))
		//
		//})
		//
		//It("Invalid parallel count", func() {
		//	args := CreateTestArgsInvalidParallelCount()
		//	err := args.Validate()
		//	Expect(err).To(Not(BeNil()))
		//
		//})
		//
		//It("Invalid file name", func() {
		//	args := CreateTestArgsInvalidFileName()
		//	err := args.Validate()
		//	Expect(err).To(Not(BeNil()))
		//
		//})
		//
		//It("Invalid max depth", func() {
		//	args := CreateTestArgsInvalidMaxDepth()
		//	err := args.Validate()
		//	Expect(err).To(Not(BeNil()))
		//
		//})
	})
})

func CreateTestUrl() *url.URL {
	u, _ := url.Parse(ValidUrl)

	return u
}

func CreateTestGenerator() *Generator {

	return NewGenerator(CreateTestUrl())
}
