package main

import (
	"context"
	"fmt"
	"log"
	"os"
	"runtime"

	"gitlab.com/aurox_task/sitemap/args"

	cli "github.com/jawher/mow.cli"
)

const (
	serviceName        = "Sitemap Generator"
	serviceDescription = "Simple sitemap (https://www.sitemaps.org) generator as command line tool."
	serviceVer         = "1.0.1"
)

func main() {
	app := cli.App("App", serviceDescription)
	argOpt := &args.Args{}

	app.Version("V version", fmt.Sprintf("%s v%s", serviceName, serviceVer))

	argOpt.Url = app.StringArg("URL", "", "Website url, if given without scheme it's reading as http://")
	argOpt.ParallelCount = app.IntOpt("parallel", runtime.NumCPU(), fmt.Sprintf("Number of parallel workers to navigate through site [0, %d], default is num CPU", args.MaxParallelCount))
	argOpt.OutputFile = app.StringOpt("output-file", "sitemap.xml", "Output file name")
	argOpt.MaxDepth = app.IntOpt("max-depth", 2, fmt.Sprintf("Max depth of url navigation recursion, [0, %d]", args.MaxMaxDepth))
	argOpt.IsServer = app.BoolOpt("server", false, "Server mode with gracefully shutdown")

	ctx, cancel := context.WithCancel(context.Background())
	service := NewService(ctx, cancel, argOpt)

	app.Action = service.Start

	if err := app.Run(os.Args); err != nil {

		log.Fatalf("%s failed start: %v", serviceName, err)
	}
}
