.DEFAULT_GOAL := build

.PHONY: build
build:
	CGO_ENABLED=1 GO111MODULE=on GOPROXY=direct GOSUMDB=off GOARCH=amd64 go build -o ./build/sitemap

.PHONY: test
test:
	CGO_ENABLED=1 GO111MODULE=on GOPROXY=direct GOSUMDB=off GOARCH=amd64 go test -count=1 `go list ./...`

.PHONY: clear
clear:
	rm ./build/sitemap
