package args

import (
	"os"
	"testing"

	. "github.com/onsi/ginkgo"
	. "github.com/onsi/gomega"
	"gitlab.com/aurox_task/sitemap/utils"
)

const ValidUrl = "https://name.com"
const InvalidUrl = "https://[name].com"
const ValidFileName = "sitemap.xml"
const InvalidFileName = string(os.PathSeparator)

func TestArgs(t *testing.T) {
	RegisterFailHandler(Fail)
	RunSpecs(t, "Postgres_Test")
}

var _ = Describe("Args", func() {
	Context("External Args", func() {
		It("Valid args", func() {
			args := CreateTestArgsValid()
			err := args.Validate()
			Expect(err).To(BeNil())

		})

		It("Invalid url", func() {
			args := CreateTestArgsInvalidUrl()
			err := args.Validate()
			Expect(err).To(Not(BeNil()))

		})

		It("Invalid parallel count negative", func() {
			args := CreateTestArgsInvalidParallelCount1()
			err := args.Validate()
			Expect(err).To(Not(BeNil()))

		})

		It("Invalid parallel count greater limit", func() {
			args := CreateTestArgsInvalidParallelCount2()
			err := args.Validate()
			Expect(err).To(Not(BeNil()))

		})

		It("Invalid file name", func() {
			args := CreateTestArgsInvalidFileName()
			err := args.Validate()
			Expect(err).To(Not(BeNil()))

		})

		It("Invalid max depth negative", func() {
			args := CreateTestArgsInvalidMaxDepth1()
			err := args.Validate()
			Expect(err).To(Not(BeNil()))

		})

		It("Invalid max depth greater limit", func() {
			args := CreateTestArgsInvalidMaxDepth2()
			err := args.Validate()
			Expect(err).To(Not(BeNil()))

		})
	})
})

func CreateTestArgsValid() *Args {

	return &Args{
		Url:           utils.StringToP(ValidUrl),
		ParallelCount: utils.IntToP(0),
		OutputFile:    utils.StringToP(ValidFileName),
		MaxDepth:      utils.IntToP(0),
		IsServer:      utils.BoolToP(true),
	}
}

func CreateTestArgsInvalidUrl() *Args {

	return &Args{
		Url:           utils.StringToP(InvalidUrl),
		ParallelCount: utils.IntToP(0),
		OutputFile:    utils.StringToP(ValidFileName),
		MaxDepth:      utils.IntToP(0),
		IsServer:      utils.BoolToP(true),
	}
}

func CreateTestArgsInvalidParallelCount1() *Args {

	return &Args{
		Url:           utils.StringToP(ValidUrl),
		ParallelCount: utils.IntToP(-1),
		OutputFile:    utils.StringToP(ValidFileName),
		MaxDepth:      utils.IntToP(0),
		IsServer:      utils.BoolToP(true),
	}
}

func CreateTestArgsInvalidParallelCount2() *Args {

	return &Args{
		Url:           utils.StringToP(ValidUrl),
		ParallelCount: utils.IntToP(MaxParallelCount + 1),
		OutputFile:    utils.StringToP(ValidFileName),
		MaxDepth:      utils.IntToP(0),
		IsServer:      utils.BoolToP(true),
	}
}

func CreateTestArgsInvalidFileName() *Args {

	return &Args{
		Url:           utils.StringToP(ValidUrl),
		ParallelCount: utils.IntToP(0),
		OutputFile:    utils.StringToP(InvalidFileName),
		MaxDepth:      utils.IntToP(0),
		IsServer:      utils.BoolToP(true),
	}
}

func CreateTestArgsInvalidMaxDepth1() *Args {

	return &Args{
		Url:           utils.StringToP(ValidUrl),
		ParallelCount: utils.IntToP(0),
		OutputFile:    utils.StringToP(ValidFileName),
		MaxDepth:      utils.IntToP(-1),
		IsServer:      utils.BoolToP(true),
	}
}

func CreateTestArgsInvalidMaxDepth2() *Args {

	return &Args{
		Url:           utils.StringToP(ValidUrl),
		ParallelCount: utils.IntToP(0),
		OutputFile:    utils.StringToP(ValidFileName),
		MaxDepth:      utils.IntToP(MaxMaxDepth + 1),
		IsServer:      utils.BoolToP(true),
	}
}
