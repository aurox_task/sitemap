Sitemap generator
---------------------

Implement simple sitemap (https://www.sitemaps.org) generator as command line tool.
Please implement this test task in the same way as you would do it for production
code, which means pay attention to edge cases and details.

It should:

* accept start url as argument
* recursively navigate by site pages in parallel
* should not use any external dependencies, only standard golang library
* extract page urls only from `<a>` elements and take in account `<base>` element if
declared

should be well tested (automated testing)

Suggested program options:
- `--parallel` number of parallel workers to navigate through site
- `--output-file` output file path
- `--max-depth` max depth of url navigation recursion
- `--server` server mode

Server mode application
=====================

It's starting as server:
* running until complete the task
* able to interrupt via POSIX signal, then start is gracefully shutdown (as possible with external library)
* logger adapt to json format for kubernetes

Command line mode application
=====================

It's starting as server:
* running until complete the task
* able to immediately interrupt via POSIX signal
* standard logger


Build and testing
=====================

Makefile helps to build and running tests


Running example
=====================
* `sitemap --parallel 2 https://ridus.ru`
* `sitemap --parallel 2 https://xn--d1aqf.xn--p1ai/`
* `sitemap --server --parallel 2 --max-depth 2 ridus.ru`
* `sitemap --parallel 2 --max-depth 3 ridus.ru/`

