package generator

import (
	"errors"
	"io"
	"os"
	"sort"
	"strings"
	"sync"
)

const MaxLocLen = 2048
const MaxUrls = 50000
const MaxSiteMapLen = 52428800 - 9 // 52,428,800 - len("</urlset>")
const Indent = "\t"

// Generator building sitemap
type Generator struct {
	locs map[string]struct{}
	mu   *sync.Mutex
}

func NewGenerator() *Generator {

	return &Generator{
		locs: make(map[string]struct{}),
		mu:   new(sync.Mutex),
	}
}

// AddLoc append new path of website
func (m *Generator) AddLoc(uri string) (bool, error) {
	m.mu.Lock()
	defer m.mu.Unlock()

	if len(uri) >= MaxLocLen {
		err := errors.New("loc must be less than 2,048 characters")

		return false, err
	} else if _, ok := m.locs[uri]; !ok {
		m.locs[uri] = struct{}{}

		if len(m.locs) > MaxUrls {
			err := errors.New("must have no more than 50,000 urls")

			return false, err
		} else {

			return true, nil
		}
	} else {

		return false, nil
	}
}

// sort is prettify output data
func (m *Generator) sort() []string {
	urls := make([]string, 0, len(m.locs))

	for loc := range m.locs {
		urls = append(urls, loc)

	}

	sort.Slice(urls, func(i, j int) bool {
		return urls[i] < urls[j]
	})

	return urls
}

// WriteToFile write to the file
func (m *Generator) WriteToFile(fileName string) error {
	if f, err := os.OpenFile(fileName, os.O_APPEND|os.O_CREATE|os.O_WRONLY, 0644); err != nil {

		return err
	} else if err := m.writeTo(f); err != nil {

		return err
	} else {

		return nil
	}
}

// writeTo format as https://sitemaps.org/protocol.html
func (m *Generator) writeTo(w io.StringWriter) (err error) {
	locs := m.sort()
	b := &strings.Builder{}

	err = m.write(b, `<?xml version="1.0" encoding="UTF-8"?>`, err)
	err = m.write(b, "\n", err)
	err = m.write(b, `<urlset xmlns="http://www.sitemaps.org/schemas/sitemap/0.9">`, err)
	err = m.write(b, "\n", err)

	for _, loc := range locs {
		err = m.write(b, Indent, err)
		err = m.write(b, `<url>`, err)
		err = m.write(b, "\n", err)

		err = m.write(b, Indent, err)
		err = m.write(b, Indent, err)
		err = m.write(b, `<loc>`, err)
		err = m.write(b, loc, err)
		err = m.write(b, `</loc>`, err)
		err = m.write(b, "\n", err)

		err = m.write(b, Indent, err)
		err = m.write(b, `</url>`, err)
		err = m.write(b, "\n\n", err)

		if b.Len() > MaxSiteMapLen {
			err = errors.New("sitemap must be no larger than 50MB (52,428,800 bytes)")

			break

		}
	}

	err = m.write(b, `</urlset>`, err)

	if err == nil {
		_, err = w.WriteString(b.String())

	}

	return
}

// write help to accumulate error
func (m *Generator) write(b *strings.Builder, s string, e error) (err error) {
	if e == nil {
		_, err = b.WriteString(s)

	} else {
		err = e

	}

	return
}

// LenLoc checking loc count in sitemap
func (m *Generator) LenLoc() int {

	return len(m.locs)
}
