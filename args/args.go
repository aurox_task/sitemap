package args

import (
	"errors"
	"fmt"
	"io/ioutil"
	"net/url"
	"os"
	"runtime"

	"gitlab.com/aurox_task/sitemap/utils"
)

const MaxMaxDepth = 10
const MaxParallelCount = 100

type Args struct {
	Url           *string
	ParallelCount *int
	OutputFile    *string
	MaxDepth      *int
	IsServer      *bool
}

// UseZeroAsUnlimited setup zeros in default
func (m *Args) UseZeroAsUnlimited() {
	if *m.ParallelCount == 0 {
		m.ParallelCount = utils.IntToP(runtime.NumCPU())

	}

	if *m.MaxDepth == 0 {
		m.MaxDepth = utils.IntToP(10)

	}
}

// Validate of input args
func (m *Args) Validate() (err error) {
	if *m.Url == "" {
		err = errors.New("url is required")

	} else if _, errParse := url.Parse(*m.Url); errParse != nil {
		err = errors.New(fmt.Sprintf("failed parse url: %v", errParse))

	} else if *m.ParallelCount < 0 {
		err = errors.New("number of parallel workers is negative")

	} else if *m.ParallelCount > MaxParallelCount {
		err = errors.New(fmt.Sprintf("number of parallel workers not be greater then %d", MaxParallelCount))

	} else if errFile := m.ValidateFileName(); errFile != nil {
		err = errFile

	} else if *m.MaxDepth < 0 {
		err = errors.New("max depth is negative")

	} else if *m.MaxDepth > MaxMaxDepth {
		err = errors.New(fmt.Sprintf("max depth not be greater then %d", MaxMaxDepth))

	}

	return
}

// ValidateFileName if exist or given symbol not support by OS
func (m *Args) ValidateFileName() (err error) {
	if _, errStat := os.Stat(*m.OutputFile); errStat == nil {
		err = errors.New(fmt.Sprintf("%s already exist", *m.OutputFile))

	} else {
		var d []byte
		if errWrite := ioutil.WriteFile(*m.OutputFile, d, 0644); errWrite != nil {
			err = errWrite

		} else if errRemove := os.Remove(*m.OutputFile); errRemove != nil { // And delete it

			panic("app finished incorrectly")

		} else {
			// OK!
		}
	}

	return
}

func (m *Args) GetUrl() *url.URL {
	if m.Url == nil {

		return nil
	} else if u, err := url.Parse(*m.Url); err != nil {

		return nil
	} else {

		return u
	}
}
