package parser

import (
	"fmt"
	"net/url"
	"strings"
	"sync"

	"github.com/gocolly/colly"
	"github.com/gocolly/colly/extensions"
	"github.com/sirupsen/logrus"
	"gitlab.com/aurox_task/sitemap/generator"
)

type Service struct {
	logger        *logrus.Logger
	url           *url.URL
	parallelCount int
	maxDepth      int
	gen           *generator.Generator
	allowHosts    []string
	lastError     error
	inShutdown    bool
	mu            *sync.Mutex // for using lastError, inShutdown
}

func NewService(logger *logrus.Logger, url *url.URL, parallelCount, maxDepth int) *Service {
	s := &Service{
		logger:        logger,
		url:           url,
		parallelCount: parallelCount,
		maxDepth:      maxDepth,
		gen:           generator.NewGenerator(),
		allowHosts:    nil,
		lastError:     nil,
		inShutdown:    false,
		mu:            new(sync.Mutex),
	}

	s.makeWwwHost()

	return s
}

func (s *Service) Serve() {
	c := colly.NewCollector(colly.MaxDepth(s.maxDepth), colly.Async(true))
	extensions.RandomUserAgent(c)

	// find and visit all links
	c.OnHTML("a[href]", func(e *colly.HTMLElement) {
		if !s.isLastError() && !s.InShutdown() {
			// uri made using base[href] https://github.com/gocolly/colly/blob/master/colly.go#L1078
			uri := e.Request.AbsoluteURL(e.Attr("href"))

			if u, err := url.Parse(uri); err != nil {
				s.logger.Error(err)

			} else if s.isAllowHost(u.Host) && s.isAllowScheme(u.Scheme) {
				if ok, err := s.gen.AddLoc(uri); err != nil {
					s.logger.Error(err)
					s.setLastError(err)

				} else if ok && e.Request.Depth < s.maxDepth {
					if err := e.Request.Visit(uri); err != nil { // async mode err will not catch here
						s.logger.Error(err)

					}
				}
			}
		}
	})

	// visiting info
	c.OnRequest(func(r *colly.Request) {
		s.logger.Infof("visiting %s", r.URL.String())

	})

	// error warning
	c.OnError(func(r *colly.Response, err error) {
		s.logger.Warn(err)

	})

	if err := c.Limit(&colly.LimitRule{DomainGlob: "*", Parallelism: s.parallelCount}); err != nil {
		s.logger.Error(err)
		s.setLastError(err)

	} else {
		if err := c.Visit(s.url.String()); err != nil {
			s.logger.Warn(err)

		}

		c.Wait()

	}
}

func (s *Service) WriteToFile(fileName string) {
	if !s.isLastError() && !s.InShutdown() {
		if s.gen.LenLoc() > 1 {
			if err := s.gen.WriteToFile(fileName); err != nil {
				s.logger.Error(err)
				s.setLastError(err)

			}
		} else {
			s.logger.Info("sitemap is empty")
		}
	}
}

func (s *Service) isAllowHost(host string) bool {
	for _, h := range s.allowHosts {
		if host == h {

			return true
		}
	}

	return false
}

func (s *Service) isAllowScheme(scheme string) bool {
	if s.url.Scheme == "" || scheme == s.url.Scheme {

		return true
	} else {

		return false
	}
}

func (s *Service) makeWwwHost() {
	woWww := strings.TrimPrefix(s.url.Host, "www.")

	s.allowHosts = append(s.allowHosts, woWww, fmt.Sprintf("www.%s", woWww))
}

func (s *Service) StartShutdown() {
	s.mu.Lock()
	defer s.mu.Unlock()

	s.inShutdown = true

}

// InShutdown switch inShutdown <- true
func (s *Service) InShutdown() bool {
	s.mu.Lock()
	defer s.mu.Unlock()

	return s.inShutdown

}

func (s *Service) setLastError(err error) {
	s.mu.Lock()
	defer s.mu.Unlock()

	s.inShutdown = true
	s.lastError = err

}

func (s *Service) isLastError() bool {
	s.mu.Lock()
	defer s.mu.Unlock()

	return s.lastError != nil

}
