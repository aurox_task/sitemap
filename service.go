package main

import (
	"context"
	"os"
	"os/signal"
	"sync"
	"syscall"

	"github.com/sirupsen/logrus"
	"gitlab.com/aurox_task/sitemap/args"
	"gitlab.com/aurox_task/sitemap/logger"
	"gitlab.com/aurox_task/sitemap/parser"
)

type Service struct {
	ctx    context.Context
	Cancel context.CancelFunc
	logger *logrus.Logger
	args   *args.Args
	parser *parser.Service
}

func NewService(ctx context.Context, cancel context.CancelFunc, args *args.Args) *Service {
	return &Service{
		ctx:    ctx,
		Cancel: cancel,
		logger: nil,
		args:   args,
	}
}

// Start is main loop after start application
func (s *Service) Start() {
	s.logger = logger.InitLogger(*s.args.IsServer)

	s.args.UseZeroAsUnlimited()

	if err := s.args.Validate(); err != nil {
		s.logger.Fatal(err)

	}

	if s.args.Url == nil || s.args.OutputFile == nil || s.args.ParallelCount == nil || s.args.MaxDepth == nil {
		s.logger.Fatal("args validator is incorrect implement") // never

	}

	if *s.args.IsServer {
		s.Serve()

	} else {
		s.Run()

	}
}

func (s *Service) Run() {
	u := s.args.GetUrl()

	s.parser = parser.NewService(s.logger, u, *s.args.ParallelCount, *s.args.MaxDepth)
	s.parser.Serve()
	s.parser.WriteToFile(*s.args.OutputFile)

}

func (s *Service) Serve() {
	u := s.args.GetUrl()

	wg := &sync.WaitGroup{}
	finish := make(chan struct{}, 1) // not blocking channel for normal finish

	wg.Add(1)
	go func() {
		defer wg.Done()

		<-s.ctx.Done()
		s.logger.Debugf("main services is start shutdown...")
		s.Shutdown()
	}()

	s.parser = parser.NewService(s.logger, u, *s.args.ParallelCount, *s.args.MaxDepth)

	wg.Add(1)
	go func() {
		defer wg.Done()

		s.parser.Serve()
		s.parser.WriteToFile(*s.args.OutputFile)
		s.logger.Debugf("sitemap generator is done.")
		finish <- struct{}{}
		s.logger.Debugf("sitemap generator is finished.")

	}()

	s.WaitForShutdown(wg, s.Cancel, finish)
}

// WaitForShutdown Serve and gracefully Shutdown
func (s *Service) WaitForShutdown(wg *sync.WaitGroup, cancel context.CancelFunc, finish chan struct{}) {
	defer s.logger.Debugf("main services is shutdown.")

	sigChan := make(chan os.Signal, 1)
	signal.Notify(sigChan, syscall.SIGTERM, syscall.SIGINT)

	wg.Add(1)
	go func() {
		defer wg.Done()
		defer cancel()

		select {
		case <-sigChan:
			s.logger.Debugf("main services is interrupt.")

		case <-finish:
			s.logger.Debugf("main services is normal finish.")
		}
	}()

	wg.Wait()
}

// Shutdown send to Sitemap Parser shutdown flag
func (s *Service) Shutdown() {
	if s.parser != nil {
		s.parser.StartShutdown()

	}
}
