package utils

import (
	"testing"

	. "github.com/onsi/ginkgo"
	. "github.com/onsi/gomega"
)

const BoolValid = true
const IntValid = 2
const StrValid = "sda"

func TestArgs(t *testing.T) {
	RegisterFailHandler(Fail)
	RunSpecs(t, "Utils_Test")
}

var _ = Describe("Utils", func() {
	Context("Utils", func() {
		It("Utils bool", func() {
			i := BoolToP(BoolValid)
			Expect(i).To(Not(BeNil()))

		})

		It("Utils int", func() {
			i := IntToP(IntValid)
			Expect(i).To(Not(BeNil()))

		})

		It("Utils str", func() {
			s := StringToP(StrValid)

			Expect(s).To(Not(BeNil()))

		})
	})
})
